import React, { Component } from "react";
import "./App.scss";

import Table from "./components/Table/Table";
import Spinner from "./components/Spinner/Spinner";

class App extends Component {
  state = {
    error: null,
    isLoading: false,
    users: [],
    searchField: "",
    fillteredData: [],
    sortedData: [],
    sortDirection: true,
    searchingData: false,
    sortingData: false,
    tableHeadTitles: [
      { id: "fullName", text: "Name", value: "" },
      { id: "balance", text: "Balance", value: "" },
      { id: "isActive", text: "Active", value: "" },
      { id: "registered", text: "Registered", value: "" },
      { id: "state", text: "State", value: "" },
      { id: "country", text: "Country", value: "" },
    ],
  };

  componentDidMount() {
    this.setState({ isLoading: true });
    fetch(
      "https://gitlab.com/urosuros45/fww-table/-/raw/master/src/data/data.json"
    )
      .then((res) => {
        if (res.ok) {
          return res.json();
        } else {
          throw new Error("Something went wrong...");
        }
      })
      .then((data) => {
        let users = [];
        data.map((element) => {
          // country
          let e = element.state;
          let country = element.country;

          return e.map((s) => {
            //state
            let stateName = s.name;
            let usr = s.users;
            return usr.map((user) => {
              return users.push({ ...user, country, state: stateName });
            });
          });
        });

        return this.setState({ users: users, isLoading: false });
      })
      .catch((error) => this.setState({ error, isLoading: false }));
  }

  sortBy = (key) => {
    let users = [...this.state.users];
    let user = users.map((user) => user);
    console.log(this.state.sortDirection);
    let sortDirection = this.state.sortDirection;
    if(sortDirection){
      let sortAsc = user.sort((a, b) =>
      a[key] > b[key] ? 1 : b[key] > a[key] ? -1 : 0);
    this.setState({ sortedData: sortAsc, sortingData: true });
    }else {
      let sortDesc = user.sort((a, b) =>
        a[key] < b[key] ? 0 : b[key] > a[key] ? 1 : -1
       );
       this.setState({ sortedData: sortDesc, sortingData: true });
    }
    this.setState({sortDirection: !sortDirection});
  };
  clearSearchValue = (e) => {
    const { tableHeadTitles } = this.state;
    let id = e.target.id;
    tableHeadTitles.forEach((tht) => {
      if (id !== tht.id) {
        tht.value = "";
      }
    });
    this.setState({ searchField: "", searchingData: false });
    // let users = [...this.state.users];
  };

  searchDataHandler = (e) => {
    let id = e.target.id;
    const { searchField } = this.state;
    let users = [...this.state.users];
    const { tableHeadTitles } = this.state;
    let fillteredData = [...this.state.fillteredData];

    this.setState({ searchField: e.target.value, searchingData: true });
    fillteredData = users.filter((user) =>
      typeof user[id] === "string"
        ? user[id].toLowerCase().includes(searchField.toLowerCase())
        : user[id].toString().toLowerCase().includes(searchField.toLowerCase())
    );
    this.setState({ fillteredData: fillteredData });
    let sf = "";
    sf += e.target.value;
    tableHeadTitles.forEach((tht) => {
      if (id === tht.id) {
        tht.value = sf;
      }
    });
  };

  render() {
    const {
      users,
      isLoading,
      error,
      tableHeadTitles,
      fillteredData,
      searchingData,
      searchField,
      sortedData,
      sortingData,
      sortDirection
    } = this.state;
   
    if (error) {
      return <p>{error.message}</p>;
    }

    if (isLoading) {
      return <Spinner />;
    }
    return (
      <div className="App">
        <Table
          users={users}
          tableHeadTitles={tableHeadTitles}
          searchDataHandler={this.searchDataHandler}
          fillteredData={fillteredData}
          searchingData={searchingData}
          sortBy={this.sortBy}
          searchField={searchField}
          clearSearchValue={this.clearSearchValue}
          sortedData={sortedData}
          sortingData={sortingData}
          sortDirection={sortDirection}
        />
      </div>
    );
  }
}

export default App;
